# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.0

- minor: Added sections for dot net project files.  Pom files and assmebly info files are not yet implemented.

## 0.1.0

- minor: Initial release - Not Ready for Produciton Use

