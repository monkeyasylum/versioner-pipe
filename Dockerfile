FROM alpine:3.9

RUN apk add --update --no-cache bash libxslt jq curl

#RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh
COPY LICENSE.txt pipe.yml README.md /
COPY pipe /

RUN chmod a+x /*.sh

#ENTRYPOINT ["/pipe.sh"]
CMD ["/pipe.sh"]
