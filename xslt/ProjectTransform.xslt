﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="dotNetVer"/>
	<xsl:param name="semVer"/>
	<xsl:param name="configuration"/>
	<xsl:param name="projectUrl"/>
	<xsl:param name="repositoryUrl"/>
	<xsl:param name="repositoryType"/>
	<xsl:param name="branch"/>
	<xsl:param name="commit"/>
	<xsl:param name="releaseNotes"/>

	<xsl:output method="xml" omit-xml-declaration="yes" indent="yes" />

	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup[1]">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
			<xsl:if test="count(FileVersion) = 0">
				<xsl:message>Adding FileVersion Property</xsl:message>
				<xsl:call-template name="FileVersion" />
				<xsl:text>
</xsl:text>
			</xsl:if>
			<xsl:if test="count(InformationalVersion) = 0">
				<xsl:message>Adding InformationalVersion Property</xsl:message>
				<xsl:call-template name="InformationalVersion" />
				<xsl:text>
</xsl:text>
			</xsl:if>
			<xsl:if test="count(PackageVersion) = 0">
				<xsl:message>Adding PackageVersion Property</xsl:message>
				<xsl:call-template name="PackageVersion" />
				<xsl:text>
</xsl:text>
			</xsl:if>
			<xsl:if test="count(Configuration) = 0">
				<xsl:message>Adding Configuration Property</xsl:message>
				<xsl:call-template name="Configuration" />
				<xsl:text>
</xsl:text>
			</xsl:if>
			<xsl:if test="count(PackageProjectUrl) = 0">
				<xsl:message>Adding PackageProjectUrl Property</xsl:message>
				<xsl:call-template name="PackageProjectUrl" />
				<xsl:text>
</xsl:text>
			</xsl:if>
			<xsl:if test="count(RepositoryUrl) = 0">
				<xsl:message>Adding RepositoryUrl Property</xsl:message>
				<xsl:call-template name="RepositoryUrl" />
				<xsl:text>
</xsl:text>
			</xsl:if>
			<xsl:if test="count(RepositoryType) = 0">
				<xsl:message>Adding RepositoryType Property</xsl:message>
				<xsl:call-template name="RepositoryType" />
				<xsl:text>
</xsl:text>
			</xsl:if>
			<xsl:if test="count(RepositoryBranch) = 0">
				<xsl:message>Adding RepositoryBranch Property</xsl:message>
				<xsl:call-template name="RepositoryBranch" />
				<xsl:text>
</xsl:text>
			</xsl:if>
			<xsl:if test="count(RepositoryCommit) = 0">
				<xsl:message>Adding RepositoryCommit Property</xsl:message>
				<xsl:call-template name="RepositoryCommit" />
				<xsl:text>
</xsl:text>
			</xsl:if>
			<xsl:if test="count(PackageReleaseNotes) = 0">
				<xsl:message>Adding PackageReleaseNotes Property</xsl:message>
				<xsl:call-template name="PackageReleaseNotes" />
				<xsl:text>
</xsl:text>
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/FileVersion">
		<xsl:message>Updating FileVersion Property</xsl:message>
		<xsl:call-template name="FileVersion"/>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/InformationalVersion">
		<xsl:message>Updating InformationalVersion Property</xsl:message>
		<xsl:call-template name="InformationalVersion"/>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/PackageVersion">
		<xsl:message>Updating PackageVersion Property</xsl:message>
		<xsl:call-template name="PackageVersion"/>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/Configuration">
		<xsl:message>Updating Configuration Property</xsl:message>
		<xsl:call-template name="Configuration"/>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/PackageProjectUrl">
		<xsl:message>Updating PackageProjectUrl Property</xsl:message>
		<xsl:call-template name="PackageProjectUrl"/>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/RepositoryUrl">
		<xsl:message>Updating RepositoryUrl Property</xsl:message>
		<xsl:call-template name="RepositoryUrl"/>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/RepositoryType">
		<xsl:message>Updating RepositoryType Property</xsl:message>
		<xsl:call-template name="RepositoryType"/>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/RepositoryBranch">
		<xsl:message>Updating RepositoryBranch Property</xsl:message>
		<xsl:call-template name="RepositoryBranch"/>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/RepositoryCommit">
		<xsl:message>Updating RepositoryCommit Property</xsl:message>
		<xsl:call-template name="RepositoryCommit"/>
	</xsl:template>

	<xsl:template match="/Project/PropertyGroup/PackageReleaseNotes">
		<xsl:message>Updating PackageReleaseNotes Property</xsl:message>
		<xsl:call-template name="PackageReleaseNotes"/>
	</xsl:template>

	<xsl:template name="FileVersion">
		<xsl:element name="FileVersion">
			<xsl:message>Setting FileVersion to '<xsl:value-of select="$dotNetVer"/>'</xsl:message>
			<xsl:value-of select="$dotNetVer"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="InformationalVersion">
		<xsl:element name="InformationalVersion">
			<xsl:message>Setting InformationalVersion to '<xsl:value-of select="$semVer"/>'</xsl:message>
			<xsl:value-of select="$semVer"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="PackageVersion">
		<xsl:element name="PackageVersion">
			<xsl:message>Setting PackageVersion to '<xsl:value-of select="$semVer"/>'</xsl:message>
			<xsl:value-of select="$semVer"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Configuration">
		<xsl:element name="Configuration">
			<xsl:message>Setting Configuration to '<xsl:value-of select="$configuration"/>'</xsl:message>
			<xsl:value-of select="$configuration"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="PackageProjectUrl">
		<xsl:element name="PackageProjectUrl">
			<xsl:message>Setting PackageProjectUrl to '<xsl:value-of select="$projectUrl"/>'</xsl:message>
			<xsl:value-of select="$projectUrl"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="RepositoryUrl">
		<xsl:element name="RepositoryUrl">
			<xsl:message>Setting RepositoryUrl to '<xsl:value-of select="$repositoryUrl"/>'</xsl:message>
			<xsl:value-of select="$repositoryUrl"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="RepositoryType">
		<xsl:element name="RepositoryType">
			<xsl:message>Setting RepositoryType to '<xsl:value-of select="$repositoryType"/>'</xsl:message>
			<xsl:value-of select="$repositoryType"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="RepositoryBranch">
		<xsl:element name="RepositoryBranch">
			<xsl:message>Setting RepositoryBranch to '<xsl:value-of select="$branch"/>'</xsl:message>
			<xsl:value-of select="$branch"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="RepositoryCommit">
		<xsl:element name="RepositoryCommit">
			<xsl:message>Setting RepositoryCommit to '<xsl:value-of select="$commit"/>'</xsl:message>
			<xsl:value-of select="$commit"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="PackageReleaseNotes">
		<xsl:element name="PackageReleaseNotes">
			<xsl:message>Setting PackageReleaseNotes to '<xsl:value-of select="$releaseNotes"/>'</xsl:message>
			<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
			<xsl:value-of select="$releaseNotes"/>
			<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
