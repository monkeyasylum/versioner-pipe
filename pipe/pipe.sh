#!/usr/bin/env bash
#
# Bitbucket pipe to set version numbers in .Net assemblies.
#

source "$(dirname "$0")/logging.sh"
source "$(dirname "$0")/atlas-utils.sh"

trap '$(error "${BASH_COMMAND}\n\treturned ${?} in ${FUNCNAME:-pipe.sh} at ${LINENO}"; status=98;)' ERR

info "Executing the pipe..."
status=99
# enable_debug

# Test parameters
# DEBUG=true
# BITBUCKET_BRANCH=${BITBUCKET_BRANCH:="feature/MM-12-create-a-list-view"}
# #BITBUCKET_BRANCH=${BITBUCKET_BRANCH:="master"}
# BITBUCKET_WORKSPACE=${BITBUCKET_WORKSPACE:="monkeyasylum"}
# BITBUCKET_REPO_SLUG=${BITBUCKET_REPO_SLUG:="movie-manager-ui"}

info "Validating parameters..."
# Required parameters
#NAME=${NAME:?'NAME variable missing.'}

# Default parameters
MODE=${MODE:="Generate"}
TARGET=${TARGET:="Project"}
DEBUG=${DEBUG:="false"}
PROD_BRANCH=${PROD_BRANCH:='^master$'}
BETA_BRANCH=${BETA_BRANCH:='^release/.+'}
MAJOR_RELEASE_PATTERN=${MAJOR_RELEASE_PATTERN:='(BREAKING CHANGE|Major Release)$'}
MINOR_RELEASE_PATTERN=${MINOR_RELEASE_PATTERN:='(Add Feature|New Feature|Minor Release)$'}
PROJECT_URL=${PROJECT_URL:="https://bitbucket.org/$BITBUCKET_WORKSPACE/workspace/projects/MM"}
REPO_TYPE=${REPO_TYPE:="git (Bitbucket)"}
PROJECT_FILE_FOLDER=${PROJECT_FILE_FOLDER:="$BITBUCKET_PIPE_SHARED_STORAGE_DIR/projectfiles"}

# Shared parameters
info "Setting shared parameters..."
BRANCH=${BITBUCKET_PR_DESTINATION_BRANCH:-$BITBUCKET_BRANCH}
debug "BRANCH = $BRANCH"

shopt -s nocasematch
if [[ $BRANCH =~ $PROD_BRANCH ]]; then
  RELEASE_LEVEL="PROD";
else if [[ $BRANCH =~ $BETA_BRANCH ]]; then
  RELEASE_LEVEL="BETA";
else
  RELEASE_LEVEL="ALPHA";
fi fi
debug "RELEASE_LEVEL = $RELEASE_LEVEL"

function generate(){
  info "Retrieving branch commits"
  debug "curl -s -X GET https://api.bitbucket.org/2.0/repositories/$BITBUCKET_WORKSPACE/$BITBUCKET_REPO_SLUG/commits/$BRANCH"
  commits=$(curl -s -X GET https://api.bitbucket.org/2.0/repositories/$BITBUCKET_WORKSPACE/$BITBUCKET_REPO_SLUG/commits/$BRANCH)
  debug "# commits = $(echo $commits | jq .pagelen)"
  # TODO - add error checking/handling
  debug "Saving commits to '$BITBUCKET_PIPE_STORAGE_DIR/commits.json'"
  echo $commits > "$BITBUCKET_PIPE_STORAGE_DIR/commits.json"
  info "Finding last build tag"
  find-last-tag "$commits"
  lastBuildTag="$atlas_utils_retval"
  # TODO - add error checking/handling
  info "Last build tag is: $(echo $lastBuildTag | jq .name)"
  generate-version "$lastBuildTag" "$commits"
  newVersion=(${atlas_utils_retval[@]})
  debug "newVersion = ${newVersion[@]}"
  # TODO - add error checking/handling
  info "New version tag is: ${newVersion[$VERSION_TAG]}"
  debug "Saving version # to '$BITBUCKET_PIPE_STORAGE_DIR/version.json'"
  echo "${newVersion[@]}" > "$BITBUCKET_PIPE_STORAGE_DIR/version.json"
  debug "saved version = $(cat "$BITBUCKET_PIPE_STORAGE_DIR/version.json")"
  find-last-tag "$commits" true
  lastReleaseTag="$atlas_utils_retval"
  # TODO - add error checking/handling
  info "Last release tag is: $(echo $lastReleaseTag | jq .name)"
  generate-notes "$commits" "$lastReleaseTag" "${newVersion[@]}"
  # TODO - add error checking/handling
  status=0
}

function tag(){
  info "Loading version from '$BITBUCKET_PIPE_STORAGE_DIR/version.json'"
  local newVersion="cat '$BITBUCKET_PIPE_STORAGE_DIR/version.json'"
  echo "Committing updated files to the repository..."
  git add .
  git commit -m "Update files for new version '${newVersion[$VERSION_TAG]}' [skip ci]"
  git push origin ${BITBUCKET_BRANCH}

  echo "Tagging for release ${newVersion[$VERSION_TAG]}" "${newVersion[$VERSION_TAG]}"
  git tag -a -m "Tagging for release ${newVersion[$VERSION_TAG]}" "${newVersion[$VERSION_TAG]}"
  git push origin ${newVersion[$VERSION_TAG]}
  status=0
}

case $MODE in
  "Generate") generate;;
  "Tag") tag;;
  "Test") success "Test Ok!"; status=0;;
  *) fail "Unknown Mode '$MODE' supplied!"
esac

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error! Status is $status"
fi

#region Powershell Code

# #region AssmeblyInfo
function update-assembly-info() {
    warn "update-assembly-info is not yet implemented"
# Write-Host "Transforming AssemblyInfo.* Files..."
# $AllVersionFiles = Get-ChildItem $srcPath AssemblyInfo.* -Include *.cs,*.vb -recurse

# foreach ($file in $AllVersionFiles){
#     $path = $file.FullName
#     Write-Host "Transforming $path"

# 	$assemblyVersion = Select-String -Path $file.FullName -Pattern "^[^']*AssemblyVersion\(""([0-9]+)\.([0-9]+)\.[^""]+""\)"
# 	$hasAssemblyFileVersion = Select-String -Path $file.FullName -Pattern "^[^']*AssemblyFileVersion\(.*\)"
# 	$hasAssemblyConfiguration = Select-String -Path $file.FullName -Pattern "^[^']*AssemblyConfiguration\(.*\)"
# 	$hasAssemblyInformationalVersion = Select-String -Path $file.FullName -Pattern "^[^']*AssemblyInformationalVersion\(.*\)"
#     Write-Verbose "Matches: $assemblyVersion"
#     $major = $assemblyVersion.Matches[0].Groups[1].Value
#     $minor = $assemblyVersion.Matches[0].Groups[2].Value
#     $assemblyFileVersion = "{0}.{1}.{2}.{3}" -f $major, $minor, $julianDate, $buildIncrementalNumber
#     Write-Host "Transformed Assembly File Version is $assemblyFileVersion"
#     if($informationalAsSemvar){
#         $assemblyInformationalVersion = "{0}.{1}.{2}{3:00}{4}" -f $major, $minor, $julianDate, $buildIncrementalNumber, $label
#         Write-Host "Transformed Assembly Informational Version is $assemblyInformationalVersion"
#     }

# 	#version replacements
# 	(Get-Content $file.FullName) |
# 	%{$_ -replace 'AssemblyFileVersion\("[^"]*"\)', "AssemblyFileVersion(""$assemblyFileVersion"")" } |
# 	%{$_ -replace 'AssemblyConfiguration\("[^"]*"\)', "AssemblyConfiguration(""$assemblyConfiguration"")"} |
# 	%{$_ -replace 'AssemblyInformationalVersion\("[^"]*"\)', "AssemblyInformationalVersion(""$assemblyInformationalVersion"")"} |
# 	Set-Content $file.FullName -Force
#     if ($file.Extension -eq ".vb"){
#         $assemblyFileVersionAttribute = '<Assembly: AssemblyFileVersion("{0}")>' -f $assemblyFileVersion
#         $assemblyConfigurationAttribute = '<Assembly: AssemblyConfiguration("{0}")>' -f $assemblyConfiguration
#         $assemblyInformationalVersionAttribute = '<Assembly: AssemblyInformationalVersion("{0}")>' -f $assemblyInformationalVersion
#     }
#     else {
#         $assemblyFileVersionAttribute = '[assembly: AssemblyFileVersion("{0}")]' -f $assemblyFileVersion
#         $assemblyConfigurationAttribute = '[assembly: AssemblyConfiguration("{0}")]' -f $assemblyConfiguration
#         $assemblyInformationalVersionAttribute = '[assembly: AssemblyInformationalVersion("{0}")]' -f $assemblyInformationalVersion
#     }
#     if(!$hasAssemblyFileVersion){
# 		Add-Content $file.FullName $assemblyFileVersionAttribute -Force
#     }
#     if(!$hasAssemblyConfiguration){
# 		Add-Content $file.FullName $assemblyConfigurationAttribute -Force
#     }
#     if(!$hasAssemblyInformationalVersion){
# 		Add-Content $file.FullName $assemblyInformationalVersionAttribute -Force
#     }
# }
# Write-Host "Transformed AssemblyInfo.* Files."
}
# #endregion

# #region ProjectFiles
function updated-project-files() {
  info "Transforming Project Files..."

  for file in "$(find "$PROJECT_FILE_FOLDER" -type f -iname *\.(vb|cs)proj)"; do {
    local path = $file.FullName
    info "Transforming $path..."
    xsltproc --stringparam "dotNetVer" "${version[$VERSION_MAJOR]}.${version[$VERSION_MINOR]}.${version[$VERSION_PATCH]}.0" \
             --stringparam "semVer" "${version[$VERSION_MAJOR]}.${version[$VERSION_MINOR]}.${version[$VERSION_PATCH]}${version[$VERSION_SUFFIX]}" \
             --stringparam "configuration" "$BUILD_CONFIG" \
             --stringparam "projectUrl" "$PROJECT_URL" \
             --stringparam "repositoryUrl" "https://bitbucket.org/$BITBUCKET_REPO_FULL_NAME" \
             --stringparam "repositoryType" "$REPO_TYPE" \
             --stringparam "branch" "$BRANCH" \
             --stringparam "commit" "$BITBUCKET_COMMIT" \
             --stringparam "releaseNotes" "$releaseNotes" \
             -o "$path" "./xslt/ProjectTransform.xslt" "$path"
    success "Transformed $path"
  }; done
  info "Transformed Project Files."
}
# #endregion

# #region PomFiles
function updated-pom-files() {
    warn "update-pom-files is not yet implemented"
# Write-Host "Transforming POM Files..."
# $AllPomFiles = Get-ChildItem $srcPath pom.xml -recurse
# $transformOutputPath = [IO.Path]::Combine($env:Agent_TempDirectory, "XslOutput.txt")

# $xslt = New-Object System.Xml.Xsl.XslCompiledTransform
# $xslt.Load([IO.Path]::Combine($PWD.Path, "PomTransform.xslt"), [System.Xml.Xsl.XsltSettings]::TrustedXslt, $null)
# $xsltArgs = New-Object System.Xml.Xsl.XsltArgumentList
# $xsltArgs.AddParam("julianDate", "", $julianDate)
# $xsltArgs.AddParam("buildIncrementalNumber", "", ("{0:00}" -f $buildIncrementalNumber))
# $xsltArgs.AddParam("infoVersion", "", $label)
# $xsltArgs.AddParam("configuration", "", $assemblyConfiguration)
# $xsltArgs.AddParam("logFile", "", $transformOutputPath)

# foreach ($file in $AllPomFiles){
#     $path = $file.FullName
#     Write-Host "Transforming $path"
#     $stream = New-Object System.IO.MemoryStream
#     $xslt.Transform($path, $xsltArgs, $stream)
#     $writer = [System.IO.FileStream]::new($path, [System.IO.FileMode]::Create, [System.IO.FileAccess]::Write)
#     $stream.WriteTo($writer)
#     $writer.Flush()
#     $writer.Close()
#     $writer.Dispose()
#     $stream.Close()
#     $stream.Dispose()
# }

# #region Generate Dependency Transform
# if(-not [string]::IsNullOrEmpty($transformOutputPath) -and (Test-Path -Path $transformOutputPath)){
#     $dependencyTransformPath = [IO.Path]::Combine($PWD.Path, "DependencyTransform.xslt")
#     Copy-Item -Path $dependencyTransformPath -Destination $env:Agent_TempDirectory -Force
#     $dependencyTransformPath = [IO.Path]::Combine($env:Agent_TempDirectory, "DependencyTransform.xslt")
#     $newVersions = Get-Content -Path $transformOutputPath
#     foreach ($versionDescriptor in $newVersions){
#         $versionInfo = $versionDescriptor.Split('|')
#         $groupId = $versionInfo[0]
#         $artifactId = $versionInfo[1]
#         $version = $versionInfo[2]
#         if(-not [String]::IsNullOrEmpty($versionInfo[0])){
#             $transform = @"
# 	<xsl:template match="maven:parent[maven:groupId='$groupId' and maven:artifactId='$artifactId']/maven:version">
# 		<xsl:message>Updating Parent '<xsl:value-of select="../maven:groupId" />, <xsl:value-of select="../maven:artifactId"/>' Version to $version</xsl:message>
# 		<xsl:element name="version" namespace="http://maven.apache.org/POM/4.0.0">
# 			<xsl:text>$version</xsl:text>
# 		</xsl:element>
# 	</xsl:template>
# 	<xsl:template match="maven:dependency[maven:groupId='$groupId' and maven:artifactId='$artifactId']/maven:version">
# 		<xsl:message>Updating Dependency '<xsl:value-of select="../maven:groupId" />, <xsl:value-of select="../maven:artifactId"/>' Version to $version</xsl:message>
# 		<xsl:element name="version" namespace="http://maven.apache.org/POM/4.0.0">
# 			<xsl:text>$version</xsl:text>
# 		</xsl:element>
# 	</xsl:template>
# "@
#             $transform | Out-File -LiteralPath $dependencyTransformPath -Append -Encoding ascii
#         }
#     }
# @"
# </xsl:stylesheet>
# "@ | Out-File -LiteralPath $dependencyTransformPath -Append -Encoding ascii
# #endregion

#     $xslt = New-Object System.Xml.Xsl.XslCompiledTransform
#     $xslt.Load($dependencyTransformPath)#, [System.Xml.Xsl.XsltSettings]::TrustedXslt, $null)

#     foreach ($file in $AllPomFiles){
#         $path = $file.FullName
#         Write-Host "Transforming $path"
#         $stream = New-Object System.IO.MemoryStream
#         $xslt.Transform($path, $null, $stream)
#         $writer = [System.IO.FileStream]::new($path, [System.IO.FileMode]::Create, [System.IO.FileAccess]::Write)
#         $stream.WriteTo($writer)
#         $writer.Flush()
#         $writer.Close()
#         $writer.Dispose()
#         $stream.Close()
#         $stream.Dispose()
#     }
# }
}
# #endregion

# Write-Host -ForegroundColor Yellow "Set Assembly Versions"
# Write-Host "##vso[task.complete result=Succeeded;]Set Assembly Versions Complete"
#################
#endregion
