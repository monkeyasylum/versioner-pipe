# Bitbucket Pipelines Pipe: Versioner

Bitbucket pipe to set version numbers in .Net assemblies.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: monkeyasylum/versioner:0.2.0
    variables:
      NAME: "<string>"
      MODE: "Project|AssemblyInfo|Pom" # Default: Project
      # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| NAME (*)              | The name that will be printed in the logs                   |
| MODE                  | The version info file mode.                                 |
|                       |   Project - (default) This will update the values in Visual |
|                       |             Studio Project files (*.vbproj, *.csproj)       |
|                       |   AssemblyInfo - This will update the values in             |
|                       |                  AssemblyInfo.* files                       |
|                       |   Pom - This will update Maven pom.xml files. (Not Implemented)|
| DEBUG                 | Turn on extra debug information. Default: `false`.          |

_(*) = required variable._

## Prerequisites

## Examples

Basic example:

```yaml
script:
  - pipe: monkeyasylum/versioner:0.2.0
    variables:
      NAME: "foobar"
```

Advanced example:

```yaml
script:
  - pipe: monkeyasylum/versioner:0.2.0
    variables:
      NAME: "foobar"
      DEBUG: "true"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by davidcutler@rogers.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
