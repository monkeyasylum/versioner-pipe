#!/bin/bash

#######################################
# Constants indexing the "version" array
#######################################
readonly VERSION_PRERELEASE=4
readonly VERSION_PATCH=3
readonly VERSION_MINOR=2
readonly VERSION_MAJOR=1
readonly VERSION_TAG=0
readonly VERSION_LEVEL_TEXT=("Tag" "Major" "Minor" "Patch" "PreRelease")

#######################################
# Generates release notes
# Globals:
#   BITBUCKET_PIPE_SHARED_STORAGE_DIR
#   RELEASE_LEVEL
# Arguments:
#   commits - JSON of all of the commits for the branch (order by descending date)
#   previousReleaseTag - JSON for the tag found (usually via find-last-tag) for the previous release
#   newVersion - The array describing the new version number (see generate-version)
# Returns:
#   atlas_utils_retval - Will contain the plain text release notes that can be inserted into version specs.
#######################################
function generate-notes() {
    shopt -s nocasematch
    local commits=${1:?"$(fail 'generate-notes - A list of commits must be provided!')"}; shift;
    local previousReleaseTag=${1:?"$(fail 'generate-notes - A previous release tag must be provided!')"}; shift;
    local newVersion=("$@")
    newVersion=${newVersion:?"$(fail 'generate-notes - A new version must be provided!')"}
    local lastReleaseDate=$(echo $previousReleaseTag | jq -r .target.date)
    debug "lastReleaseDate = $lastReleaseDate"

    info "Generating notes for version ${newVersion[$VERSION_MAJOR]}.${newVersion[$VERSION_MINOR]}.${newVersion[$VERSION_PATCH]}${newVersion[$VERSION_PRERELEASE]}"
    local changeLogNotes="## ${newVersion[$VERSION_MAJOR]}.${newVersion[$VERSION_MINOR]}.${newVersion[$VERSION_PATCH]}${newVersion[$VERSION_PRERELEASE]}\n"
    local releaseNotes=''
    local applicableCommits=$(echo $commits | jq ".values[] | select(.date > \"$lastReleaseDate\")" | jq -c -s)
    for i in `seq 1 $(echo $applicableCommits | jq length)`; do {
        if [[ ! "$(echo $applicableCommits | jq -r .[$i-1].message)" =~ ^merged ]]; then
            debug "Adding commit : '$(echo $applicableCommits | jq -r .[$i-1].hash)'"
            changeLogNotes+="\n$(echo $applicableCommits | jq -r .[$i-1].rendered.message.html)"
            releaseNotes+="\n$(echo $applicableCommits | jq -r .[$i-1].message)"
        fi
    } done;
    local changeLogFilename="$BITBUCKET_PIPE_SHARED_STORAGE_DIR/CHANGELOG-$RELEASE_LEVEL.md"
    changeLogFilename=${changeLogFilename//-PROD/}
    if [[ ! -f "$changeLogFilename" ]]; then
        info "Creating '$changeLogFilename'"
        echo -e '# Changelog\nNote: version releases in the 0.x.y range may introduce breaking changes.\n' > "$changeLogFilename"
    fi
    info "Updating '$changeLogFilename'"
    echo -e "$changeLogNotes\n" | sed -i -e '3r/dev/stdin' "$changeLogFilename"
    atlas_utils_retval="$releaseNotes"
}

#######################################
# Locates the most recent tag on a Bitbucket repo
# Globals:
#   BITBUCKET_WORKSPACE
#   BITBUCKET_REPO_SLUG
#   BRANCH
#   RELEASE_LEVEL
# Arguments:
#   commits - JSON of all of the commits for the branch (order by descending date)
#   byReleaseLevel - true to filter tags based on relese level.  This will enable locating the most recent tag within a release stream (such as the Prod stream).
# Returns:
#   atlas_utils_retval - JSON for the found tag
#######################################
function find-last-tag {
    shopt -s nocasematch
    local commits=${1:?"$(fail 'find-last-tag - A list of commits must be provided!')"}
    local byReleaseLevel=${2:-false}
    local lastTagCommitHash=''
    local tagFilter=''

    if [ $byReleaseLevel == true ]; then
        info "Checking release level"
        if [ $RELEASE_LEVEL == 'PROD' ]; then
            # Set the filter to exclude alpha and beta
            tagFilter='&q=name!~"beta"+AND+name!~"alpha"'
        else if [ $RELEASE_LEVEL == 'BETA' ]; then
            # Set the filter to exclude alpha
            tagFilter='&q=name!~"alpha"'
        fi fi
    fi
    info "Locating last $([ $byReleaseLevel == "true" ] && echo "$RELEASE_LEVEL " || echo '')tag in branch '$BRANCH'..."
    local tags=$(curl -s -X GET https://api.bitbucket.org/2.0/repositories/$BITBUCKET_WORKSPACE/$BITBUCKET_REPO_SLUG/refs/tags?sorted=date$tagFilter)
    debug "# tags = $(echo $tags | jq .pagelen)"
    local commitHashes=$(echo $commits | jq -r .values[].hash)
    for tagTarget in $(echo $tags | jq -r .values[].target.hash); do {
        if [[ "${commitHashes[@]}" =~ "${tagTarget}" ]]; then
            lastTagCommitHash="$tagTarget"
        fi
    } done;
    debug "lastTageCommitHash = $lastTagCommitHash"
    local tag=$(echo $tags | jq ".values[] | select(.target.hash == \"$lastTagCommitHash\")" | jq -s "reverse | .[0]")
    info "Found version tag '$(echo $tag | jq -r .name)'"
    atlas_utils_retval="$tag"
}

#######################################
# Generate a new version number for a build
# Globals:
#   RELEASE_LEVEL
#   BRANCH
#   MAJOR_RELEASE_PATTERN
#   MINOR_RELEASE_PATTERN
#   BITBUCKET_BUILD_NUMBER
# Arguments:
#   buildTag - The JSON for the git version tag to base the new version on
#   commits - JSON of all of the commits for the branch (order by descending date)
# Returns:
#   atlas_utils_retval - Contains an area decribing the new version number.  The array is indexed by the constants VERSION_TAG, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_PRERELEASE (in that order)
#######################################
function generate-version() {
    shopt -s nocasematch
    local buildTag=${1:?"$(error 'generate-version - A build tag must be provided!')"}
    local commits=${2:?"$(error 'generate-version - A list of commits must be provided!')"}
    local lastBuildDate=$(echo $buildTag | jq -r .target.date)
    local lastBuildVersion=$(echo $buildTag | jq -r .name)
    debug "lastBuildDate = $lastBuildDate"

    info "Determining the version level"
    if [[ "$lastBuildVersion" =~ /-$RELEASE_LEVEL\./ ]]; then
        VERSION_LEVEL=$VERSION_PRERELEASE
    else
        local applicableCommits=$(echo $commits | jq ".values[] | select(.date > \"$lastBuildDate\")" | jq -c -s)
        for i in `seq 1 $(echo $applicableCommits | jq length)`; do {
            if [[ "$(echo ${applicableCommits[$i]} | jq .message)" =~ $MAJOR_RELEASE_PATTERN ]]; then
                info "Identified a Major Release in commit $(echo ${applicableCommits[$i]} | jq .hash)"
                VERSION_LEVEL=$VERSION_MAJOR
            else if [[ "${applicableCommits[$i]}" =~ $MINOR_RELEASE_PATTERN ]]; then
                info "Identified a Minor Release in commit $(echo ${applicableCommits[$i]} | jq .hash)"
                VERSION_LEVEL=${VERSION_LEVEL:-$VERSION_MAJOR}
            fi fi
        } done;
    fi
    VERSION_LEVEL=${VERSION_LEVEL:=$VERSION_PATCH}
    info "The version level is set to '${VERSION_LEVEL_TEXT[$VERSION_LEVEL]}'"

    info "Parsing the current version and incrementing based on version level"
    local prerelease=''
    case $RELEASE_LEVEL in
        "BETA") prerelease="-beta.$BITBUCKET_BUILD_NUMBER+$BRANCH";;
        "ALPHA") prerelease="-alpha.$BITBUCKET_BUILD_NUMBER+$BRANCH";;
        *) prerelease='';;
    esac
    prerelease=${prerelease//\//"."}
    [[ "$lastBuildVersion" =~ v([0-9]+)\.([0-9]+)\.([0-9]+) ]]
    local version=("v${BASH_REMATCH[$VERSION_MAJOR]}.${BASH_REMATCH[$VERSION_MINOR]}.${BASH_REMATCH[$VERSION_PATCH]}$prerelease" ${BASH_REMATCH[$VERSION_MAJOR]} ${BASH_REMATCH[$VERSION_MINOR]} ${BASH_REMATCH[$VERSION_PATCH]} "$prerelease")
    if $VERSION_LEVEL =< $VERSION_PATCH ; then
        version[$VERSION_LEVEL]+=1
        for i in $(seq $VERSION_LEVEL 2); do
            version[$i+1]=0
        done
    fi
    version[$VERSION_TAG]="v${version[$VERSION_MAJOR]}.${version[$VERSION_MINOR]}.${version[$VERSION_PATCH]}${version[$VERSION_PRERELEASE]}"
    info "Parsed and set new version"

    atlas_utils_retval="${version[@]}"
}
